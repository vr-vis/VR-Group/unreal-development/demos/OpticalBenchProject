// Fill out your copyright notice in the Description page of Project Settings.


#include "OpticalBench423GameModeBase.h"

#include "OptiXModule.h"
#include "OptiXPlayerController.h"

AOpticalBench423GameModeBase::AOpticalBench423GameModeBase(const FObjectInitializer& ObjectInitializer)
{
	PlayerControllerClass = AOptiXPlayerController::StaticClass();
}

void AOpticalBench423GameModeBase::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	// Init the context
	if (GetWorld() == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("GetWorld was null in gamemode"));
	}
	FOptiXModule::Get().Init();
}

const TArray<FSceneData>& AOpticalBench423GameModeBase::GetOptiXSceneDataArray()
{
	return FOptiXModule::Get().GetSceneDataArray();
}